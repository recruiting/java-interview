Explanation:

Socrates is a man. All men are mortal. Therefore, Socrates is mortal.

You probably know that a general inference engine may carry out this sort of reasoning by combining the knowledge encapsulated in a rule like Human(x) -> Mortal(x) with the knowledge of the fact Human(Socrates) to infer Mortal(Socrates).

But in this exercise, rules refer only to concrete propositions given as simple unitary variables, not quantified expressions. The conjunction of all the premises appearing on the left hand side of a rule implies that the single conclusion appearing on the right hand side can be derived. We can represent rules of this form in EDN like so: [[:socrates-is-a-man :all-men-are-mortal] :-> :socrates-is-mortal].

Given this set of two rules:

[[:a :b] :-> :c] [[:c :d :e] :-> :f

If we are given the premises :a and :b, we can derive :c as the only resulting conclusion of the rule system. If we are given :a, :b, :d, and :e, we can use the fact that we can derive :c to also derive :f.

Although a variable on the right-hand side of a rule can appear on the left-hand side of other rules, we only consider variables that appear solely on left-hand sides to be inputs to the rule system as a whole; any variable appearing on a right-hand side is considered an output. (There may be more than one rule that can be used to derive the same output, but you can assume there are no cycles by which an output would contribute to the implication of itself.) Here, :a, :b, :d, and :e are the valid inputs to the rule system, and :c and :f are the possible outputs.

Task 1. Write a function that takes a set of rules, and partitions all the variables involved into a set of input variables and a set of output variables, according to the above definitions.

Task 2. Write a function that performs basic inference on rule systems of this kind. It should take two arguments: a set of rules and a set of input assertions, and it should return the set of all outputs that the rule system can derive from those inputs. You will be asked to develop a more sophisticated approach to inference in task 5, but do take this task seriously.

Please be prepared for further discussion.

We would like to see the design decisions you make.

Feel free to implement your solution in any JVM-based language.