```
The School of Languages and Science teaches five subjects: Physics, Chemistry, Math, Botany, and Zoology. There are currently n students enrolled in the school, and each student is skilled in only one subject. The skills of the students are described by string of length n named skills that consists of the letters p, c, m, b, and z only. Each character i describes the skill of student i as follows:
 
p denotes the student is skilled in Physics.
c denotes the student is skilled in Chemistry.
m denotes the student is skilled in Math.
b denotes the student is skilled in Botany.
z denotes the student is skilled in Zoology.
 
For example, if skills is mcmz, then the school has four students where two students are skilled at Math, one student is skilled at Chemistry, and one student is skilled at Zoology.
 
Complete the differentTeams function in the editor. It has one parameter: a string, skills. The function must return an integer denoting the total number of different teams satisfying the following constraints:
 
A team consists of a group of exactly five students.
Each student is skilled in a different subject.
Each of the students in the team must not be part of any other team.
 
Input Format
Locked stub code in the editor reads a string denoting skills from stdin and passes it to the function.
 
Constraints
5 ≤ n ≤ 5 × 105
skills consists of the letters p, c, m, b, and z only.
 
Output Format
The function must return an integer denoting the total number of different five-student teams such that each student is skilled in a different subject and each of the students must not be part of any other team.
 
Sample Input 0
pcmbz
 
Sample Output 0
1
 
Explanation 0
The students' skills are described as follows:
 
The first student is skilled in Physics.
The second student is skilled in Chemistry.
The third student is skilled in Math.
The fourth student is skilled in Botany.
The fifth student is skilled in Zoology.
 
We can form only one team of five students such that each student is skilled in a different subject, so the function returns 1. Note that because we can only form one team, we are guaranteed that at least one student will only be part of one team (as all the students are only on the one team).
 
Sample Input 1
pcmpp
 
Sample Output 1
0
 
Explanation 1
The students' skills are described as follows:
 
The first student is skilled in Physics.
The second student is skilled in Chemistry.
The third student is skilled in Math.
The fourth student is skilled in Physics.
The fifth student is skilled in Physics.
 
As there are no students skilled in Botany and Zoology, so it's impossible to form any team and the function returns 0.
 
Sample Input 2
pcmpcmbbbzz
 
Sample Output 2
2
 
Explanation 2
The students' skills are described as follows:
 
The first student is skilled in Physics.
The second student is skilled in Chemistry.
The third student is skilled in Math.
The fourth student is skilled in Physics.
The fifth student is skilled in Chemistry.
The sixth student is skilled in Math.
The seventh student is skilled in Botany.
The eighth student is skilled in Botany.
The ninth student is skilled in Botany.
The tenth student is skilled in Zoology.
The eleventh student is skilled in Zoology.
 
We can form the following two teams:
 
The first team consists of the first, second, third, seventh, and tenth students.
The second team consists of the fourth, fifth, sixth, eighth, and eleventh students.
 
Note that this is not the only way to form the two teams, but all possible ways will only ever form two teams. Thus, the function returns 2.
```
