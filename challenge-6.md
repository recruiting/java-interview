Pre-screening questions
--------------------

1. Explain what 'path to root' means in the context of garbage collection. What are roots?

2. Write code for a simple implementation of HashMap/Hashtable

3. Write a short program to illustrate the concept of deadlock

4. Explain why recursive implementation of QuickSort will require O(log n) of additional space

5. Explain the design pattern used in Java and .NET io stream/reader APIs.


Code Test
---------

Create an Iterator filtering framework:

(1) IObjectTest interface with a single boolean test(Object o) method

(2) An implementation of Iterator (let's call it FilteringIterator) which is initialized with another Iterator and an IObjectTest instance: new FilteringIterator(myIterator, myTest).

Your FilteringIterator will then allow iteration over 'myIterator', but skipping any objects which don't pass the 'myTest' test. Create a simple unit test for this framework.
