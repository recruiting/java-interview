**Coding Challenge**

The effort to fully sequence the human genome, the Human Genome Project, began in 1990 and completed in 2000 at a cost of $2.7 billion. Gathering information from various scientists, one of the problems the project dealt with was assembling small pieces of DNA ("reads") into larger sequences. This problem is known as the "assembly problem".

Today the human genome can be sequenced for around $1000, and the cost continues to drop. But we still deal with the assembly problem today.

**Part A**

Write a function, longestOverlap, that determines the length of the longest overlap between two lines of DNA. The suffix of one line has to overlap the prefix of the other.

For example:

     read1: TGAGTGGA
     read2: AAGGTGAG

Aligned, the reads look like this:
    
         TGAGTGGA
     AAGGTGAG
     
    
**Part B**

Look at lambda_virus.txt. Each line is a read from a real virus genome but they're in no particular order.

We put a mistake in the reads! Mwhahaha. Using the function from part A, determine which line of DNA belongs to a different organism (i.e. it doesn’t assemble well into this genome).
