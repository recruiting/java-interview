Sample questions for microservices interview:

1. 	What is a Microservice?
2. 	What are the benefits of Microservices architecture?
3. 	What is the role of architect in Microservices architecture?
4. 	What is the advantage of Microservices architecture over Service Oriented Architecture (SOA)?
5. 	Is it a good idea to provide a Tailored Service Template for Microservices development in an organization?
6. 	What are the disadvantages of using Shared libraries approach to decompose a monolith application?
7. 	What are the characteristics of a Good Microservice?
8. 	What is Bounded Context?
9. 	What are the points to remember during integration of Microservices?
10.	Is it a good idea for Microservices to share a common database?
11. 	What is the preferred type of communication between Microservices? Synchronous or Asynchronous?
12. 	What is the difference between Orchestration and Choreography in Microservices architecture?
13. 	What are the issues in using REST over HTTP for Microservices?
14.	Can we create Microservices as State Machines?
15.	What is Reactive Extensions?
16.	What is DRY?
17.	What is Semantic Versioning?
18.	Is it a good idea to build a Microservice or buy a commercial off the shelf software?
19.	Why do we break the Monolith software into Microservices?
20.	What is Continuous Integration?
21.	What is Continuous Delivery?
22.	What is Ubiquitous language?
23.	What is the benefit of Single Service per Host model in Microservices?
24.	What are different types of Tests for Microservices?
25.	What is Mike Cohn�s Test Pyramid?
26.	What is the difference between Mock or Stub for Microservice tests?
27.	How can we eradicate non-determinism in tests?
28.	What is a Consumer Driven Contract (CDC)?
29.	What is PACT?
30.	How can we separate Deployment from Release of Microservices?
31.	What is Canary Releasing?
32.	What is the difference between Mean Time to Repair (MTTR) and Mean Time between failures (MTBF)?
33.	How can we do cross-functional testing?
34.	What is a good tool for monitoring multiple services at a time?
35.	What is Semantic Monitoring?
36.	Why do we use Correlation IDs in Microservices architecture?
37.	What is the difference between Authentication and Authorization?
38.	How does HTTPS authentication works in Microservices?
39.	What are Client certificates?
40.	Why some big companies use API keys for providing access to public APIs?
41.	What is Confused Deputy Problem in security context?
42.	How can we secure Data at Rest in an organization?
43.	What are the different points to consider for security in Microservices architecture?
44.	What is Conway�s law?
45.	What are the important Cross-Functional Requirements to consider during the design of a Microservice?
46.	What is a Circuit Breaker pattern in the context of Microservice?
47.	What is Bulkhead design pattern?
48.	What is Idempotency of a Microservice operation?
49.	How can you scale a Database?
50.	What is Command Query Responsibility Segregation (CQRS) design pattern?
51.	How will you implement Caching in Microservice?
52.	What is CAP theorem?
53.	How will you implement Service Discovery in Microservices architecture?
54.	What is a good tool for documenting the Microservices?
55.	In which scenarios, implementing Microservices architecture is not a good idea?
56.	What are the major principles of Microservices?