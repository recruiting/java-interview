```
Consider two sentences, s and t, where t is a subsequence of s. We want to find the sequence of word(s) in s that are missing from t. For example, if s = "I like cheese" and t = "like", the sequence of missing words is ["I", "cheese"].
 
Complete the missingWords function in the editor below. It has two parameters:
A string, s, denoting a sentence of space-separated words.
A string, t, denoting a sentence of space-separated words that is also a subsequence of string s.
The function must compare the two sentences and find the word(s) in s that are missing from t. It must then return an array of strings containing any words in s that are missing from t, where each element in the array is a missing word and the elements are ordered in the same sequence in which they would need to be inserted into t to make it equal to s.
 
Input Format
Locked stub code in the editor reads the following input from stdin and passes it to the function:
The first line contains a string, s, of space-separated words.
The second line contains a string, t, of space-separated words.
 
Constraints
Strings s and t consist of English alphabetic letters (i.e., a−z and A−Z) and spaces only.
1 ≤ length of t ≤ length of s ≤ 106
1 ≤ length of any word in s or t ≤ 15
It is guaranteed that string t is a subsequence of string s.
 
Output Format
The function must return an array of strings denoting any words in s that are missing from t, where the elements occur in the same exact order in which they are missing from sentence t. This is printed to stdout by locked stub code in the editor.
 
Sample Input 0
I am using HackerRank to improve programming
am HackerRank to improve
 
Sample Output 0
I
using
programming
 
Explanation 0
The missing words are:
I
using
programming
We add these words in order to the array ["I", "using", "programming"], then return this array as our answer.
```
