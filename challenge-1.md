**Problem:**

Given two arrays of unordered numbers of unknown size, check whether both arrays contain the same set of numbers.

For example:

A = {2,5,6,3,10,7,7}

B = {2,5,5,8,10,6,4}

You solution needs to be generic as well as validatable.  

Please think about efficiency.  We are looking for a scalable solution.  If you decide to go with a brute force approach, make sure to document your thoughts on how to improve.  Please be prepared for further discussion.

We would like to see the design decisions you make.
