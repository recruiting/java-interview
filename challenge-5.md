Problem Definition

Consider the following task as the building block for an expanding project that could grow over the next few months. You will own this project as a developer and it may grow to the point that other developers will be involved in its production.

The test is not about speed or memory performance. It is simply to see how you lay out a project and your coding style.

The task

At the moment there are two file formats (tabbed and csv) that need to be mined. There will be more to come in the future. The field names are specified in the first row of each file.

Write a program to take the following request and produce the total sum of the field named "points".

The program will be invoked using: java application

where

FILE is the filename FORMAT - csv or tab

Two example datafiles are as follows:

1. Comma-delimited file:
    
        ID,NAME,POINTS
        1,JOHN,50
        2,GREG,85
        3,MARK,22
        4,LARRY,128
        5,STEVE,99
    
    2. Tab-delimited file:

            ID	NAME	POINTS
            1	JOHN	50
            2	GREG	85
            3	MARK	22
            4	LARRY	128
            5	STEVE	99